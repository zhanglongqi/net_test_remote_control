# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(442, 310)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.conn_pb = QtWidgets.QPushButton(self.centralWidget)
        self.conn_pb.setGeometry(QtCore.QRect(300, 50, 93, 27))
        self.conn_pb.setDefault(False)
        self.conn_pb.setFlat(False)
        self.conn_pb.setObjectName("conn_pb")
        self.run_pb = QtWidgets.QPushButton(self.centralWidget)
        self.run_pb.setGeometry(QtCore.QRect(300, 140, 93, 27))
        self.run_pb.setObjectName("run_pb")
        self.ip_0 = QtWidgets.QLineEdit(self.centralWidget)
        self.ip_0.setGeometry(QtCore.QRect(50, 60, 201, 31))
        self.ip_0.setObjectName("ip_0")
        self.ip_1 = QtWidgets.QLineEdit(self.centralWidget)
        self.ip_1.setGeometry(QtCore.QRect(50, 110, 201, 31))
        self.ip_1.setObjectName("ip_1")
        self.ip_2 = QtWidgets.QLineEdit(self.centralWidget)
        self.ip_2.setGeometry(QtCore.QRect(50, 170, 201, 31))
        self.ip_2.setObjectName("ip_2")
        self.label = QtWidgets.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(20, 20, 68, 19))
        self.label.setObjectName("label")
        self.local_ip = QtWidgets.QLabel(self.centralWidget)
        self.local_ip.setGeometry(QtCore.QRect(100, 20, 121, 19))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.local_ip.setFont(font)
        self.local_ip.setStyleSheet("color: rgb(255, 0, 0);")
        self.local_ip.setTextFormat(QtCore.Qt.AutoText)
        self.local_ip.setAlignment(QtCore.Qt.AlignCenter)
        self.local_ip.setObjectName("local_ip")
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 442, 24))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        self.conn_pb.clicked.connect(MainWindow.connect)
        self.run_pb.clicked.connect(MainWindow.run)
        self.statusBar.messageChanged['QString'].connect(MainWindow.status_changed)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.ip_0, self.ip_1)
        MainWindow.setTabOrder(self.ip_1, self.ip_2)
        MainWindow.setTabOrder(self.ip_2, self.conn_pb)
        MainWindow.setTabOrder(self.conn_pb, self.run_pb)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.conn_pb.setText(_translate("MainWindow", "Connect"))
        self.run_pb.setText(_translate("MainWindow", "RUN"))
        self.ip_0.setInputMask(_translate("MainWindow", "000.000.000.000"))
        self.ip_1.setInputMask(_translate("MainWindow", "000.000.000.000"))
        self.ip_2.setInputMask(_translate("MainWindow", "000.000.000.000"))
        self.label.setText(_translate("MainWindow", "Local IP:"))
        self.local_ip.setText(_translate("MainWindow", "0.0.0.0"))

