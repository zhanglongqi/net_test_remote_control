#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from utils import get_host_ip

__author__ = 'longqi'
import sys

from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow

from mainwindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):

	def status_changed(self):
		pass

	def connect(self):
		self.statusBar.showMessage('connecting all the clients')
		self.clients = []
		self.clients.append(self.ip_0.text())
		self.clients.append(self.ip_1.text())
		self.clients.append(self.ip_2.text())
		print(self.clients)

	def run(self):
		self.statusBar.showMessage('running')

	def __init__(self):
		QWidget.__init__(self)

		# set up User Interface (widgets, layout...)
		self.setupUi(self)

		self.clients = []
		self.local_ip.setText(get_host_ip())


def main(argv):
	# create Qt application
	app = QApplication(argv)

	# create main window
	wnd = MainWindow()
	wnd.show()

	# Start the app up
	sys.exit(app.exec_())


if __name__ == "__main__":
	main(sys.argv)
	print(sys.argv)
